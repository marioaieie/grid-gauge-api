# grid-gauge-api

The api is available at two URLS:

* https://ep5in5xlg4.execute-api.eu-west-1.amazonaws.com/public/grid_gauge/ will return a json file with some values and a plot
* https://ep5in5xlg4.execute-api.eu-west-1.amazonaws.com/public/grid_gauge/html will return only the plot embedded in an HTML page