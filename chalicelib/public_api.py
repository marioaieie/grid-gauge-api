import json
from datetime import datetime
import boto3
import numpy as np

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

from scipy import interpolate
from io import BytesIO
import base64

print('Loading function')


def make_graph(chart_data):
    fig, ax = plt.subplots(figsize=(6, 3))

    perc = [41., 45.]

    clean_frac = np.array(chart_data)
    times = np.arange(len(chart_data))

    # Historic data
    x = np.linspace(0, 24, 96)
    # Prediction
    x = np.append(x, np.linspace(24, 47, 24))

    # Smoothing
    spl = interpolate.UnivariateSpline(times, clean_frac, s=3)
    clean_frac = spl(x)
    times = x

    red_col = '#D14D4D'
    green_col = '#24BD26'
    slate_col = '#1F3A44'
    for i in range(len(clean_frac)):
        y = clean_frac[i:i + 2]
        color = red_col if y[0] < perc[0] else \
            (green_col if y[0] > perc[1] else slate_col)

        ls = '-' if times[i] < 24 else '.'
        ax.plot(times[i:i + 2], y, ls, color=color, lw=2)

    # Plot current value
    now = 24.
    frac_now = spl(now)
    color = red_col if frac_now < perc[0] else \
        (green_col if frac_now > perc[1] else slate_col)
    ax.plot(now, frac_now, 'o', color=color, ms=10)

    # Set x ticks and limits
    ax.xaxis.set_ticks((4.4, 24, 43.4))
    ax.xaxis.set_ticklabels(['Past 12h', 'Now', 'Next 12h'])
    ax.xaxis.set_tick_params(color='None', labelcolor=slate_col, labelsize='x-large')

    ax.set_xlim(0, 48)

    # Set y ticks and limits
    frac_mean = clean_frac.mean()
    #    yticks = np.arange( ( ( frac_mean // 10 ) % 2 ) * 10 + 10, 100, 20 )
    #    yticks = np.array( [ 0 ] + list( yticks ) + [ 100 ] ) # Make sure that we have ticks at 0% and 100%
    yticks = np.arange(0, 100, 10)
    ax.yaxis.set_ticks(yticks)
    ax.yaxis.set_ticklabels(['{:.0f}%'.format(tick) for tick in yticks])
    ax.yaxis.set_tick_params(color='None', labelcolor=slate_col, labelsize='x-large')

    ylims = max(min(frac_mean - 15, clean_frac.min() - 2), 0), \
            min(max(frac_mean + 15, clean_frac.max() + 2), 100)
    ax.set_ylim(ylims)

    ax.spines['bottom'].set_color(slate_col)
    ax.spines['bottom'].set_linewidth(1.2)
    ax.spines['top'].set_color(None)
    ax.spines['right'].set_color(None)
    ax.spines['left'].set_color(None)

    figfile = BytesIO()
    fig.savefig(figfile, bbox_inches='tight')
    figfile.seek(0)  # rewind to beginning of file

    figdata_png = base64.b64encode(figfile.getvalue())

    return figdata_png


def clean_source_graph(res_in_min=30):
    json_data = {
        'live': 100.,
        'trend': "down",  # up, down or flat
        'green_from': '',
        'green_to': '',
        'chart': [],
    }
    clean = ['hydro', 'nuclear', 'solar', 'wind']

    now = datetime.utcnow()
    now = now.replace(minute=(now.minute // res_in_min) * res_in_min, second=0, microsecond=0)

    s3 = boto3.client('s3')
    bucket_name = 'grid-gauge-data'
    try:
        response = s3.get_object(Bucket=bucket_name,
                                 Key='archive/' + now.strftime('%Y/%m/%d/%H%MZ') + '.json')

    except s3.exceptions.NoSuchKey:
        now = now.replace(minute=now.minute - 1, second=0, microsecond=0, )
        now = now.replace(minute=(now.minute // res_in_min) * res_in_min, second=0, microsecond=0)
        response = s3.get_object(Bucket=bucket_name,
                                 Key='archive/' + now.strftime('%Y/%m/%d/%H%MZ') + '.json')
    data = json.loads(response['Body'].read())
    print(now)

    for i, half_hour in enumerate(data):
        generation_mix = half_hour['generationmix']
        clean_generation = [source['perc'] for source in generation_mix if source['fuel'] in clean]
        json_data['chart'] += [sum(clean_generation)]

    json_data['live'] = json_data['chart'][24]
    prediction = sum(json_data['chart'][25:29]) / 4.
    json_data['trend'] = "down" if prediction < json_data['live'] else \
        ("up" if prediction > json_data['live'] else "flat")

    print(data[24]['from'])
    print(data[28]['from'])

    max_clean = max(json_data['chart'])
    green = [i + 24 for i, value in enumerate(json_data['chart'][24:]) if value == max_clean]
    # json_data['green_from'] = data[green[0]-2]['from']
    # json_data['green_to'] = data[green[0]+2]['from']

    json_data['graph_png'] = make_graph(json_data['chart']).decode()

    return json_data


