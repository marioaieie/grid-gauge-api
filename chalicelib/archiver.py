from datetime import datetime, timedelta
import json
import urllib.request
import boto3


def call_national_grid_API( url ):
    with urllib.request.urlopen(url) as response:
        data = json.loads( response.read().decode() )['data'][:48]
    if len(data) == 0:
        raise KeyError
    return data


def archive_generation_mix(now, res_in_min=30):
    # Round to res_in_min resolution
    now = now.replace(minute=(now.minute//res_in_min)*res_in_min, second=0, microsecond=0)

    to_day = (now + timedelta(hours=12, minutes=1)).strftime('%Y-%m-%dT%H:%MZ')
    data = call_national_grid_API('https://api.carbonintensity.org.uk/generation/' + to_day + '/pt24h')

    s3 = boto3.client('s3')
    try:
        response = s3.put_object(Bucket='grid-gauge-data',
                                 Key='archive/'+now.strftime('%Y/%m/%d/%H%MZ')+'.json',
                                 Body=json.dumps(data))
        if response['ResponseMetadata']['HTTPStatusCode'] != 200:
            raise Exception('Status code is not 200')
    except:
        print('Failed to upload to S3', end='')
        raise
    else:
        print('Data uploaded to S3', end='')
        return now
    finally:
        print(' at {}'.format(datetime.now().isoformat()))
        


