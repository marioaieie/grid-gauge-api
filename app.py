import json
from datetime import datetime
from chalice import Chalice, Response
from chalicelib.public_api import clean_source_graph
from chalicelib.archiver import archive_generation_mix

app = Chalice(app_name='grid_gauge')

# Fixes crappy internet connection
# export AWS_CLIENT_TIMEOUT=900000

# Horrible hack to force chalice to add get/put to function policy
if False:
    import boto3
    s3 = boto3.client('s3')
    s3.get_object(Bucket='grid-gauge-data')
    s3.put_object(Bucket='grid-gauge-data')


@app.route('/grid_gauge', )
def index():
    """ Percentage of energy production from clean sources
    """

    json_data = clean_source_graph()

    response = {'statusCode': 200,
                'body': json.dumps(json_data),
                'headers': {'Content-Type': 'application/json', }}

    return response


@app.route('/grid_gauge/html')
def html_graph():
    """ Graph of percentage of energy production from clean sources
    """

    json_data = clean_source_graph()

    return Response(body="<html><body><img src=\"data:image/png;base64," + json_data['graph_png'] + "\"></body></html>",
                    status_code=200,
                    headers={'Content-Type': 'text/html'})


@app.schedule('cron(0/30 * * * ? *)')
def archive_national_grid_data(event):
    """ Archive national grid data to S3
    """
    return archive_generation_mix(datetime.utcnow()).isoformat()

